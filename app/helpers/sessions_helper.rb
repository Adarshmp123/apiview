module SessionsHelper

  def log_in(user, auth_token)  
    session[:user_id] = user['id']
    session[:email] = user['email']
    session[:auth_token] = auth_token
  end

  def logged_in?
    !session[:email].nil?
  end

  def log_out
    session.delete(:user_id)
    session.delete(:email)
    session.delete(:auth_token)
  end
end