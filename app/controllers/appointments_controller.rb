class AppointmentsController < ApplicationController 

  def view
    if logged_in?
      response = HospitalApi.get("/appointments")
      puts response
      if response["success"]
        @appointments = response['appointments']
        puts @appointments
      end     
      response_pat = HospitalApi.get("/patients")
      puts response_pat
      if response_pat["success"]
        @patients = response_pat['patient']
        puts @patients
      end

      response_doc = HospitalApi.get("/doctors")
      puts response_doc
      if response_doc["success"]
        @doctors = response_doc['doctor']
        puts @doctors
      end
    else
      redirect_to root_path
    end
  end


  
  



  def create    
    diseases = params[:diseases]
    patient_id = params[:patient_id]
    doctor_id = params[:doctor_id]
    response = HospitalApi.post('/appointments', body: {"appointment"=>{diseases: diseases,
              patient_id: patient_id, doctor_id: doctor_id}})
    respond_to do |format|   
      format.html {redirect_to :back}
      format.js   { render json: response.parsed_response}       
    end  
  end
end