class PatientsController < ApplicationController
   def create
    patient_id = params[:patient_id]
    name = params[:name]
    phone_number = params[:phone_number]
    response = HospitalApi.post('/patients', body: {"patient"=>{patient_id: patient_id, name: name,
               phone_number: phone_number}})
    puts response
    respond_to do |format|   
      format.html {redirect_to :back}
      format.js   { render json: response.parsed_response}       
    end   
  end
end
