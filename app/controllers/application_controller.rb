class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include SessionsHelper


class HospitalApi
    include HTTParty
    base_uri 'https://hospital-management-system-api.herokuapp.com/api/v1'    
  end

end
