class DoctorsController < ApplicationController
 
   def create   
    name = params[:name]
    phone_number = params[:phone_number]
    specialization = params[:specialization]
    response = HospitalApi.post('/doctors', body: {"doctor"=>{name: name,
              phone_number: phone_number, specialization: specialization}})
    puts response
    respond_to do |format|   
      format.html {redirect_to :back}
      format.js   { render json: response.parsed_response}       
    end   
  end
end
