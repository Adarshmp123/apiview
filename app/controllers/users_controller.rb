class UsersController < ApplicationController


  def create    
    email = params[:email]
    password = params[:password]
    password_confirmation = params[:password_confirmation]

    response = HospitalApi.post('/users', body: {"user"=>{email: email,
              password: password, password_confirmation: password_confirmation}})
    puts response
    if response['success']
      response1 = HospitalApi.post('/sessions', body: {"session"=>{email: email,
                password: password}})
      puts response1
      if response1['success']
        log_in response1['user'], response1['auth_token']
        flash[:success] = 'Successfully logged in'
        redirect_to '/appointments/view'
      else
        flash[:danger] = response['message']
        render :login
      end
    else
      puts response['errors']
      redirect_to :back

    end
  end





  def login
    redirect_to '/appointments/view', notice: 'You are already logged in.' if logged_in?
  end




  def signup
    if logged_in?
      flash[:alert] = 'You are already logged in.'
      redirect_to '/appointments/view'    
    end
  end





  def user_login
    email = params[:email]
    password = params[:password]

    response = HospitalApi.post('/sessions', body: {"session"=>{email: email,
              password: password}})
    puts response
    if response['success']
      log_in response['user'], response['auth_token']
      flash[:success] = 'Successfully logged in'
      redirect_to '/appointments/view'
    else
      flash[:danger] ='Invalid email or password'
      redirect_to :back
    end
  end






  def logout
    log_out
    redirect_to root_path, notice: 'Successfully logged out.'
  end
end
