jQuery ->
  $('#appointment-add-form').submit ->
    valuesToSubmit = $(this).serialize()
    $.ajax(
      type: 'POST'
      url: $(this).attr('action')
      data: valuesToSubmit
      dataType: 'JSON').success (e) ->
      $('#ap13').val('')
      geturl = window.location.protocol + '//' + window.location.host
      $.get geturl, (data) ->
        options = $(data).find('#appointment-list').html()
        $('#appointment-list').empty().append(options)
        options2 = $(data).find('#doctor-select-tag').html()
        $('#doctor-select-tag').empty().append(options2)
        options3 = $(data).find('#patient-select-tag').html()
        $('#patient-select-tag').empty().append(options3)
        
    false