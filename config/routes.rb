Rails.application.routes.draw do
  get 'doctors/new'
  get 'appointments/new'
  get 'appointments/view'
  get 'appointments/index'
  get 'users/signup'
  get 'users/login'
  post 'users/user_login'
  get 'users/logout'


  resources  :patients
  resources  :doctors
  resources  :appointments
  resources  :users
  
  

  
  root to: 'users#login'

end
